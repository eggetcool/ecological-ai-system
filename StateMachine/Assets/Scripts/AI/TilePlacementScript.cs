﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// TODO: 
/// </summary>
public class TilePlacementScript : MonoBehaviour {

    public int numberOfTiles;
    public int numberOfGrass;
    public int numberOfSheep;
    public int numberOfWolfs;
    
    public Sprite grassLevelOne;
    public Sprite grassLevelTwo;
    public Sprite grassLevelThree;
    public Sprite grassLevelFour;

    [SerializeField]
    private List<GameObject> m_Sheep = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_Grass = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_Wolf = new List<GameObject>();

    [HideInInspector]
    public List<GameObject> m_Tile = new List<GameObject>();
    private GameObject m_deadSheep;

    private int randX;
    private int randY;
    private int tempCount;
    private float elapsedTime;
    private bool isBlocked;
    private int rand;
    private int tmp;

    void Awake()
    {
        //Tile Spawning
        int tmp = 0;
        for (int i = 0; i < numberOfTiles; i++)
        {
            for (int j = 0; j < numberOfTiles; j++)
            {
                m_Tile.Add(Instantiate(Resources.Load("Tile", typeof(GameObject))) as GameObject);
                m_Tile[tmp].transform.position = new Vector3(j, -i, 0);
                tmp++;
            }
        }

        //Grass Spawning
        for (int i = 0; i < numberOfGrass; i++)
        {
            randX = Random.Range(0, numberOfTiles);
            randY = Random.Range(0, -numberOfTiles);

            m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
            
            if (m_Grass[i].transform.position.x == randX && m_Grass[i].transform.position.y == randY)
            {
                m_Grass.RemoveAt(m_Grass.Count - 1);
                i = 0;
                Debug.Log("DELETED GRASS");
            }
            else
                m_Grass[i].transform.position = new Vector3(randX, randY, 0);
        }

        //Sheep Spawning
        for (int i = 0; i < numberOfSheep; i++)
        {
            randX = Random.Range(0, numberOfTiles);
            randY = Random.Range(0, -numberOfTiles);

            m_Sheep.Add(Instantiate(Resources.Load("Sheep", typeof(GameObject))) as GameObject);

            if (m_Sheep[i].transform.position.x == randX && m_Sheep[i].transform.position.y == randY)
            {
                m_Sheep.RemoveAt(m_Sheep.Count - 1);
                i = 0;
                Debug.Log("DELETED SHEEP");
            }
            else
            {
                m_Sheep[i].transform.position = new Vector3(randX, randY, 0);
                m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().SetNumberOfTiles(numberOfTiles);
            }
        }

        //Wolf Spawning
        for (int i = 0; i < numberOfWolfs; i++)
        {
            randX = Random.Range(0, numberOfTiles);
            randY = Random.Range(0, -numberOfTiles);

            m_Wolf.Add(Instantiate(Resources.Load("Wolf", typeof(GameObject))) as GameObject);

            if (m_Wolf[i].transform.position.x == randX && m_Wolf[i].transform.position.y == randY)
            {
                m_Wolf.RemoveAt(m_Wolf.Count - 1);
                i = 0;
                Debug.Log("DELETED WOLF");
            }
            else
            {
                m_Wolf[i].transform.position = new Vector3(randX, randY, 0);
                m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().SetNumberOfTiles(numberOfTiles);
            }
        }
    }


	// Use this for initialization
	void Start ()
    {
        for (int i = 0; i < numberOfSheep; i++)
        {
            m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().SetSheepList = m_Sheep;
        }
        for (int i = 0; i < numberOfWolfs; i++)
        {
            m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().SetWolfsList = m_Wolf;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        elapsedTime += Time.deltaTime;
        numberOfGrass = m_Grass.Count;
        
        
        //Spreading and dying of Grass, check if sheep or wolfs are dead
        if (elapsedTime >= 1)
        {
            spreadingOfGrass();
            KillSheep();
            KillWolf();
            elapsedTime = 0;
        }
    }

    void spreadingOfGrass()
    {
        if(numberOfGrass <= 1)
        {
            //If all grass gets eaten, spawn 5 more
            for (int i = 0; i < 5; i++)
            {
                randX = Random.Range(0, numberOfTiles);
                randY = Random.Range(0, -numberOfTiles);

                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                
                if(m_Grass[i].transform.position.x == randX && m_Grass[i].transform.position.y == randY)
                {
                    Debug.Log("DELETE");
                    i -= 1;
                    m_Grass.RemoveAt(m_Grass.Count - 1);
                }
                else
                    m_Grass[i].transform.position = new Vector3(randX, randY, 0);
            }
        }

        //Check what interval the grass is at and change sprite accordingly
        for (int i = 0; i < numberOfGrass; i++)
        {
            tempCount = 0;
            isBlocked = false;
            rand = Random.Range(1, 5);

            if (m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().interval == 1)
            {
                m_Grass[i].GetComponent<SpriteRenderer>().sprite = grassLevelOne;
            }

            else if (m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().interval == 2)
            {
                m_Grass[i].GetComponent<SpriteRenderer>().sprite = grassLevelTwo;
            }

            else if (m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().interval == 3)
            {
                //Spread grass in a random tile wich is free
                m_Grass[i].GetComponent<SpriteRenderer>().sprite = grassLevelThree;
                switch (rand)
                {
                    case 1:
                        if (m_Grass[i].transform.position.y != 0 && tempCount < 1)
                        {
                            
                            tempCount = 1;
                            //Debug.Log("Grass nr " + i + " tried up");

                            for (int j = 0; j < numberOfGrass; j++)
                            {
                                if(m_Grass[j].transform.position.y == m_Grass[i].transform.position.y + 1 && m_Grass[j].transform.position.x == m_Grass[i].transform.position.x)
                                {
                                    //Debug.Log("Grass nr " + i + " was blocked");
                                    isBlocked = true;
                                }
                            }
                            if(isBlocked == false)
                            {
                                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                                m_Grass[m_Grass.Count - 1].transform.position = new Vector3(m_Grass[i].transform.position.x, m_Grass[i].transform.position.y + 1, 0);
                            }
                            isBlocked = false;
                        }
                        break;
                    case 2:
                        if (m_Grass[i].transform.position.x != 0 && tempCount < 1)
                        {
                            tempCount = 1;
                            //Debug.Log("Grass nr " + i + " tried left");

                            for (int j = 0; j < numberOfGrass; j++)
                            {
                                if(m_Grass[j].transform.position.x == m_Grass[i].transform.position.x - 1 && m_Grass[j].transform.position.y == m_Grass[i].transform.position.y)
                                {
                                    //Debug.Log("Grass nr " + i + " was blocked");
                                    isBlocked = true;
                                }
                            }
                            if (isBlocked == false)
                            {
                                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                                m_Grass[m_Grass.Count - 1].transform.position = new Vector3(m_Grass[i].transform.position.x - 1, m_Grass[i].transform.position.y, 0);
                            }
                            isBlocked = false;
                        }
                        break;
                    case 3:
                        if (m_Grass[i].transform.position.y != -(numberOfTiles - 1) && tempCount < 1)
                        {
                            tempCount = 1;
                            //Debug.Log("Grass nr " + i + " tried down");

                            for (int j = 0; j < numberOfGrass; j++)
                            {
                                if (m_Grass[j].transform.position.y == m_Grass[i].transform.position.y - 1 && m_Grass[j].transform.position.x == m_Grass[i].transform.position.x)
                                {
                                    //Debug.Log("Grass nr " + i + " was blocked");
                                    isBlocked = true;
                                }
                            }
                            if (isBlocked == false)
                            {
                                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                                m_Grass[m_Grass.Count - 1].transform.position = new Vector3(m_Grass[i].transform.position.x, m_Grass[i].transform.position.y - 1, 0);
                            }
                            isBlocked = false;
                        }
                        break;
                    case 4:
                        if (m_Grass[i].transform.position.x != (numberOfTiles - 1) && tempCount < 1)
                        {
                            tempCount = 1;
                            //Debug.Log("Grass nr " + i + " tried right");
                            for (int j = 0; j < numberOfGrass; j++)
                            {
                                if (m_Grass[j].transform.position.x == m_Grass[i].transform.position.x + 1 && m_Grass[j].transform.position.y == m_Grass[i].transform.position.y)
                                {
                                    //Debug.Log("Grass nr " + i + " was blocked");
                                    isBlocked = true;
                                }
                            }
                            if (isBlocked == false)
                            {
                                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                                m_Grass[m_Grass.Count - 1].transform.position = new Vector3(m_Grass[i].transform.position.x + 1, m_Grass[i].transform.position.y, 0);
                            }
                            isBlocked = false;
                        }
                        break;
                }
                numberOfGrass = m_Grass.Count;
            }

            else if (m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().interval == 4)
            {
                m_Grass[i].GetComponent<SpriteRenderer>().sprite = grassLevelFour;
            }

            else if (m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().interval == 5)
            {
                m_Grass[i].transform.GetComponent<GrassBehaviourScript>().dead = true;
            }

            if(m_Grass.Count != 0 && m_Grass[i].transform.GetComponent<GrassBehaviourScript>().dead == true)
            {
                m_Grass[i].transform.GetComponent<GrassBehaviourScript>().DestroyGrass();
                m_Grass.RemoveAt(i);
                numberOfGrass = m_Grass.Count;
            }
        }
    }

    public void SpawnNewSheep()
    {
        m_Sheep.Add(Instantiate(Resources.Load("Sheep", typeof(GameObject))) as GameObject);
        numberOfSheep = m_Sheep.Count;

        for (int i = 0; i < numberOfSheep; i++)
        {
            m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().SetSheepList = m_Sheep;
            m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().SetNumberOfTiles(numberOfTiles);
        }
    }

    public void SpawnNewWolf()
    {
        m_Wolf.Add(Instantiate(Resources.Load("Wolf", typeof(GameObject))) as GameObject);
        numberOfWolfs = m_Wolf.Count;

        for (int i = 0; i < numberOfWolfs; i++)
        {
            m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().SetWolfsList = m_Wolf;
            m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().SetNumberOfTiles(numberOfTiles);
        }
    }

    public void KillSheep()
    {
        for (int i = 0; i < m_Sheep.Count; i++)
        {
            if (m_Sheep.Count != 0 && m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().dead == true)
            {
                m_deadSheep = Instantiate(Resources.Load("Sheepded", typeof(GameObject))) as GameObject;
                m_deadSheep.transform.position = m_Sheep[i].transform.position;

                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                m_Grass[m_Grass.Count - 1].transform.position = m_Sheep[i].transform.position;

                m_Sheep[i].transform.GetComponent<SheepBehaviourScript>().DestroySheep();
                m_Sheep.RemoveAt(i);
                numberOfSheep = m_Sheep.Count;
                for (int j = 0; j < numberOfSheep; j++)
                {
                    m_Sheep[j].transform.GetComponent<SheepBehaviourScript>().SetSheepList = m_Sheep;
                }
            }
        }
    }

    public void KillWolf()
    {
        for (int i = 0; i < m_Wolf.Count; i++)
        {
            if (m_Wolf.Count != 0 && m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().dead == true)
            {
                m_Grass.Add(Instantiate(Resources.Load("Grass", typeof(GameObject))) as GameObject);
                m_Grass[m_Grass.Count - 1].transform.position = m_Wolf[i].transform.position;

                m_Wolf[i].transform.GetComponent<WolfBehaviourScript>().DestroyWolf();
                m_Wolf.RemoveAt(i);
                numberOfWolfs = m_Wolf.Count;
                for (int j = 0; j < numberOfWolfs; j++)
                {
                    m_Wolf[j].transform.GetComponent<WolfBehaviourScript>().SetWolfsList = m_Wolf;
                }
            }
        }
    }

    public List<GameObject> getGrass
    {
        get { return m_Grass; }
    }

    public List<GameObject> getWolfs
    {
        get { return m_Wolf; }
    }

    public List<GameObject> getSheep
    {
        get { return m_Sheep; }
    }
}
