﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassBehaviourScript : MonoBehaviour {
    enum GrassState
    {
        Idle,
        Death
    }

    public int interval;
    public int intervalTime;
    public int healthPoints;
    public bool dead;

    private float timeElapsed;
    private GrassState currentState;

	// Use this for initialization
	void Start () {
        healthPoints = Random.Range(1, 6);
        healthPoints *= 10;
        dead = false;
    }
	
	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;

        //Aging and dying
        if (timeElapsed >= intervalTime)
        {
            interval += 1;
            timeElapsed = 0;
        }

        if (healthPoints <= 0)
            currentState = GrassState.Death;

        else
            currentState = GrassState.Idle;

        CheckState();
	}

    private void CheckState()
    {
        switch (currentState)
        {
            case GrassState.Death:
                dead = true;
                break;
            case GrassState.Idle:
                dead = false;
                break;
            default:
                break;
        }
    }

    public void DestroyGrass()
    {
        //Debug.Log("I am dead");
        Destroy(gameObject);
    }

    public void EatMe(int eatDamage)
    {
        healthPoints -= eatDamage;
    }
}