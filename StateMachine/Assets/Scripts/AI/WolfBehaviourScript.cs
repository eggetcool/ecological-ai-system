﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// BUG:  ▲
///      ▲ ▲
/// </summary>


public class WolfBehaviourScript : MonoBehaviour
{
    enum WolfStates
    {
        Wander,
        Seek,
        Birth,
        Death
    }

    public int eatDamage;
    public bool dead;

    public Transform colliderChild;

    [SerializeField]
    private List<GameObject> m_Sheep = new List<GameObject>();
    private List<GameObject> m_Wolfs = new List<GameObject>();
    private List<GameObject> OGSheepList = new List<GameObject>();
    private GameObject m_TilePlacer;
    private GameObject removedSheep;

    [SerializeField]
    private float healthPoints;
    private float elapsedTime;
    private int timesSearched;
    private int timesWandered;
    private int rotationInt;
    private int lastMovement;
    private int numberOfTiles;
    private int tempCount;
    private int numberOfSheep;
    private int numberOfWolf;
    private bool isBlocked;
    private WolfStates currentState;

    // Use this for initialization
    void Start()
    {
        dead = false;
        isBlocked = false;
        healthPoints = 50f;
        rotationInt = 0;
        numberOfWolf = m_Wolfs.Count;
        m_TilePlacer = GameObject.FindGameObjectWithTag("GameController");
        OGSheepList = m_TilePlacer.GetComponent<TilePlacementScript>().getSheep;
    }

    // Update is called once per frame
    void Update()
    {
        
        numberOfWolf = m_Wolfs.Count;
        elapsedTime += Time.deltaTime;

        for (var i = m_Sheep.Count - 1; i > -1; i--)
        {
            if (m_Sheep[i] == null)
                m_Sheep.RemoveAt(i);
        }

        if (elapsedTime >= 0.5)
        {
            CheckSheep();

            //If Sheep, eat
            if (m_Sheep.Count != 0)
                currentState = WolfStates.Seek;

            //If nothing, wander random direction until find something
            if (m_Sheep.Count == 0)
                currentState = WolfStates.Wander;

            //Spawn a sheep in adjacent empty square
            if (healthPoints >= 150)
                currentState = WolfStates.Birth;

            else if (healthPoints <= 0)
                currentState = WolfStates.Death;

            CheckState();
            healthPoints -= 1;
            elapsedTime = 0;
        }


        //Screenborders
        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, 0, numberOfTiles - 1),
            Mathf.Clamp(transform.position.y, -(numberOfTiles - 1), 0));

        numberOfWolf = m_Wolfs.Count;

    }

    private void CheckState()
    {
        switch (currentState)
        {
            case WolfStates.Wander:
                WanderAround();
                break;
            case WolfStates.Seek:
                MoveTowardsSheep();
                break;
            case WolfStates.Birth:
                GiveBirth();
                break;
            case WolfStates.Death:
                dead = true;
                break;
            default:
                break;
        }
    }

    private void CheckSheep()
    {
        for (int i = 0; i < OGSheepList.Count; i++)
        {
            if ((Vector2)OGSheepList[i].transform.position == new Vector2(
                Mathf.Clamp(OGSheepList[i].transform.position.x, transform.position.x - 4, transform.position.x + 4),
                Mathf.Clamp(OGSheepList[i].transform.position.y, transform.position.y - 4, transform.position.y + 4)))
            {
                if (!m_Sheep.Contains(OGSheepList[i]))
                    m_Sheep.Add(OGSheepList[i]);
                timesSearched = 0;
            }
            else if(m_Sheep.Contains(OGSheepList[i]))
                m_Sheep.Remove(OGSheepList[i]);
        }
    }

    private void GiveBirth()
    {
        healthPoints -= 100;
        int rand = Random.Range(1, 5);
        switch (rand)
        {
            case 1:
                if (transform.position.y != 0 && tempCount < 1)
                {
                    tempCount = 1;

                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        //Occupation check
                        if (m_Wolfs[i].transform.position.y == transform.position.y + 1 && m_Wolfs[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewWolf();
                        m_Wolfs[m_Wolfs.Count - 1].transform.position = new Vector3(transform.position.x, transform.position.y + 1, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 2:
                if (transform.position.x != 0 && tempCount < 1)
                {
                    tempCount = 1;

                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        if (m_Wolfs[i].transform.position.x == transform.position.x - 1 && m_Wolfs[i].transform.position.y == transform.position.y)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewWolf();
                        m_Wolfs[m_Wolfs.Count - 1].transform.position = new Vector3(transform.position.x - 1, transform.position.y, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 3:
                if (transform.position.y != -(numberOfTiles - 1) && tempCount < 1)
                {
                    tempCount = 1;

                    for (int j = 0; j < numberOfWolf; j++)
                    {
                        if (m_Wolfs[j].transform.position.y == transform.position.y - 1 && m_Wolfs[j].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewWolf();
                        m_Wolfs[m_Wolfs.Count - 1].transform.position = new Vector3(transform.position.x, transform.position.y - 1, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 4:
                if (transform.position.x != (numberOfTiles - 1) && tempCount < 1)
                {
                    tempCount = 1;

                    for (int j = 0; j < numberOfWolf; j++)
                    {
                        if (m_Wolfs[j].transform.position.x == transform.position.x + 1 && m_Wolfs[j].transform.position.y == transform.position.y)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewWolf();
                        m_Wolfs[m_Wolfs.Count - 1].transform.position = new Vector3(transform.position.x + 1, transform.position.y, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
        }
        numberOfWolf = m_Wolfs.Count;
    }

    void MoveTowardsSheep()
    {
        if (m_Sheep.Count != 0 && m_Sheep[0] != null)
        {
            if (gameObject.transform.position.x < m_Sheep[0].transform.position.x)
            {
                //Reappearing code that checks if the new tile is preoccupied, could be optimized but because of time they are not.
                for (int i = 0; i < numberOfWolf; i++)
                {
                    if (m_Wolfs[i].transform.position.y == transform.position.y && m_Wolfs[i].transform.position.x == transform.position.x + 1)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(1, 0, 0);
                }
                isBlocked = false;
            }
            else if (gameObject.transform.position.x > m_Sheep[0].transform.position.x)
            {
                for (int i = 0; i < numberOfWolf; i++)
                {
                    if (m_Wolfs[i].transform.position.y == transform.position.y && m_Wolfs[i].transform.position.x == transform.position.x - 1)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(-1, 0, 0);
                }
                isBlocked = false;
            }

            if (gameObject.transform.position.y < m_Sheep[0].transform.position.y)
            {
                for (int i = 0; i < numberOfWolf; i++)
                {
                    if (m_Wolfs[i].transform.position.y == transform.position.y + 1 && m_Wolfs[i].transform.position.x == transform.position.x)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(0, 1, 0);
                }
                isBlocked = false;
            }
            else if (gameObject.transform.position.y > m_Sheep[0].transform.position.y)
            {
                for (int i = 0; i < numberOfWolf; i++)
                {
                    if (m_Wolfs[i].transform.position.y == transform.position.y - 1 && m_Wolfs[i].transform.position.x == transform.position.x)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(0, -1, 0);
                }
                isBlocked = false;
            }

            for (int i = 0; i < m_Sheep.Count; i++)
            {
                if (gameObject.transform.position == m_Sheep[i].transform.position)
                    EatSheep(i);
            }
        }
    }

    void EatSheep(int index)
    {
        if (m_Sheep[index] != null)
            m_Sheep[index].transform.GetComponentInParent<SheepBehaviourScript>().EatMe(eatDamage);
        healthPoints += eatDamage;
    }

    void WanderAround()
    {
        int rand = Random.Range(1, 5);

        switch (rand)
        {
            //UP
            case 1:
                if (lastMovement == 3)
                    break;
                else
                {
                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        if (m_Wolfs[i].transform.position.y == transform.position.y + 1 && m_Wolfs[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(0, 1, 0);
                    }
                    isBlocked = false;
                    lastMovement = 1;
                    break;
                }
            //LEFT
            case 2:
                if (lastMovement == 4)
                    break;
                else
                {
                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        if (m_Wolfs[i].transform.position.y == transform.position.y && m_Wolfs[i].transform.position.x == transform.position.x - 1)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(-1, 0, 0);
                    }
                    isBlocked = false;
                    lastMovement = 2;
                    break;
                }
            //DOWN
            case 3:
                if (lastMovement == 1)
                    break;
                else
                {
                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        if (m_Wolfs[i].transform.position.y == transform.position.y - 1 && m_Wolfs[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(0, -1, 0);
                    }
                    isBlocked = false;
                    lastMovement = 3;
                    break;
                }
            //RIGHT
            case 4:
                if (lastMovement == 2)
                    break;
                else
                {
                    for (int i = 0; i < numberOfWolf; i++)
                    {
                        if (m_Wolfs[i].transform.position.y == transform.position.y && m_Wolfs[i].transform.position.x == transform.position.x + 1)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(1, 0, 0);
                    }
                    isBlocked = false;
                    lastMovement = 4;
                    break;
                }
            default:
                lastMovement = 0;
                break;
        }
        timesWandered += 1;
        if (timesWandered >= 5)
        {
            timesSearched = 0;
            timesWandered = 0;
        }
    }

    public void SetNumberOfTiles(int nmbr)
    {
        numberOfTiles = nmbr;
    }

    public List<GameObject> SetWolfsList
    {
        get { return m_Wolfs; }
        set { m_Wolfs = value; }
    }

    public void DestroyWolf()
    {
        Destroy(gameObject);
    }
}
