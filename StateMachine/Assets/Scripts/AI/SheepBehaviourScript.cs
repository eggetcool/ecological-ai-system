﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// BUG: ▲
///      ▲ ▲
///      
/// TODO:   
/// </summary>


public class SheepBehaviourScript : MonoBehaviour {

    [HideInInspector]
    public enum SheepStates
    {
        Wander,
        Evade,
        Seek,
        Birth,
        Death
    }

    public int eatDamage;
    public bool dead;
    public SheepStates currentState;

    public Transform colliderChild;
    public GameObject removedGrass;
    public List<GameObject> m_Grass =       new List<GameObject>();

    [SerializeField]
    private List<GameObject> m_Wolfs =      new List<GameObject>();
    private List<GameObject> m_Sheep =      new List<GameObject>();
    [SerializeField]
    private List<GameObject> OGgrassList =  new List<GameObject>();
    [SerializeField]
    private List<GameObject> OGWolfList = new List<GameObject>();
    private GameObject m_TilePlacer;
    private GameObject TileFleeTarget;
    private Transform StaminaBar;
    private Transform WoolBar;


    [SerializeField]
    private float healthPoints;
    private float staminaPoints;
    private float WoolSize;
    private float elapsedTime;
    private int timesSearched;
    private int timesWandered;
    private int timesEvaded;
    private int rotationInt;
    private int lastMovement;
    private int numberOfTiles;
    private int tempCount;
    private int numberOfSheep;
    private int panic;
    private bool isBlocked;
    private bool gettingEaten;

    // Use this for initialization
    void Start ()
    {
        dead = false;
        isBlocked = false;
        gettingEaten = false;
        healthPoints = 50f;
        staminaPoints = 3f;
        WoolSize = 0f;
        numberOfSheep = m_Sheep.Count;
        m_TilePlacer = GameObject.FindGameObjectWithTag("GameController");
        timesEvaded = 4;

        StaminaBar = gameObject.transform.GetChild(0).GetChild(0);
        WoolBar = gameObject.transform.GetChild(0).GetChild(1);

        WoolBar.localScale = new Vector2(WoolSize / 3, WoolBar.localScale.y);


        OGgrassList = m_TilePlacer.GetComponent<TilePlacementScript>().getGrass;
        OGWolfList = m_TilePlacer.GetComponent<TilePlacementScript>().getWolfs;
    }
	
	// Update is called once per frame
	void Update ()
    {
        numberOfSheep = m_Sheep.Count;
        elapsedTime += Time.deltaTime;
        
        if(elapsedTime >= 1)
        {
            CheckForGrass();
            CheckForWolfs();

            //If wolf, run
            if (m_Wolfs.Count != 0 && gettingEaten == false)
                currentState = SheepStates.Evade;
            
            //If Grass, eat
            if (m_Grass.Count != 0 && m_Wolfs.Count == 0 && gettingEaten == false && panic <= 0)
                currentState = SheepStates.Seek;
            
            //If nothing, wander random direction until find something
            if (m_Wolfs.Count == 0 && m_Grass.Count == 0 && timesEvaded >= 4 && gettingEaten == false && panic <= 0)
                currentState = SheepStates.Wander;

            if (healthPoints >= 100 && m_Wolfs.Count != 0)
                //Spawn a sheep in adjacent empty square
                currentState = SheepStates.Birth;

            CheckForFleeingSheep();

		    if (healthPoints <= 0)
                currentState = SheepStates.Death;

            staminaPoints = Mathf.Clamp(staminaPoints, 0, 3);
            WoolSize = Mathf.Clamp(WoolSize, 0, 3);
            panic = Mathf.Clamp(panic, 0, 10);


            CheckState();
            healthPoints -= 1;
            elapsedTime = 0;

            WoolBar.localScale = new Vector2(WoolSize / 3, WoolBar.localScale.y);
            StaminaBar.localScale = new Vector2(staminaPoints / 3, WoolBar.localScale.y);
        }


        for (var i = m_Grass.Count - 1; i > -1; i--)
        {
            if (m_Grass[i] == null)
                m_Grass.RemoveAt(i);
        }
        for (var i = m_Wolfs.Count - 1; i > -1; i--)
        {
            if (m_Wolfs[i] == null)
                m_Wolfs.RemoveAt(i);
        }

        //Screenborders
        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, 0, numberOfTiles - 1),
            Mathf.Clamp(transform.position.y, -(numberOfTiles - 1), 0));

        numberOfSheep = m_Sheep.Count;

        
    }
    
    void CheckState()
    {
        switch (currentState)
        {
            case SheepStates.Wander:
                WanderAround();
                break;
            case SheepStates.Evade:
                Evation();
                break;
            case SheepStates.Seek:
                MoveTowardsGrass();
                break;
            case SheepStates.Birth:
                GiveBirth();
                break;
            case SheepStates.Death:
                dead = true;
                break;
            default:
                break;
        }
    }

    void CheckForFleeingSheep()
    {
        for (int i = 0; i < m_Sheep.Count; i++)
        {
            if(m_Sheep[i] == gameObject)
            {
                continue;
            }
            if ((Vector2)m_Sheep[i].transform.position == new Vector2(
                Mathf.Clamp(m_Sheep[i].transform.position.x, transform.position.x - 3, transform.position.x + 3),
                Mathf.Clamp(m_Sheep[i].transform.position.y, transform.position.y - 3, transform.position.y + 3)))
            {
                if (m_Sheep[i].GetComponent<SheepBehaviourScript>().currentState == SheepStates.Evade)                                                                                     
                {
                    Debug.Log("wot");
                    TileFleeTarget = GameObject.FindGameObjectWithTag("GameController").GetComponent<TilePlacementScript>().m_Tile[Mathf.Abs((int)(m_Sheep[i].transform.position.x + (transform.position.y * numberOfTiles)))];
                    currentState = SheepStates.Evade;
                    panic = 6;
                }
            }
        }
    }

    void CheckForGrass()
    {
        for (int i = 0; i < OGgrassList.Count; i++)
        {
            if ((Vector2)OGgrassList[i].transform.position == new Vector2(
                Mathf.Clamp(OGgrassList[i].transform.position.x, transform.position.x - 3, transform.position.x + 3),
                Mathf.Clamp(OGgrassList[i].transform.position.y, transform.position.y - 3, transform.position.y + 3)))
            {
                if (!m_Grass.Contains(OGgrassList[i]))
                    m_Grass.Add(OGgrassList[i]);
            }
            else if(m_Grass.Contains(OGgrassList[i]))
                m_Grass.Remove(OGgrassList[i]);

        }
    }

    void CheckForWolfs()
    {
        for (int i = 0; i < OGWolfList.Count; i++)
        {
            if ((Vector2)OGWolfList[i].transform.position == new Vector2(
                Mathf.Clamp(OGWolfList[i].transform.position.x, transform.position.x - 2, transform.position.x + 2),
                Mathf.Clamp(OGWolfList[i].transform.position.y, transform.position.y - 2, transform.position.y + 2)))
            {
                if (!m_Wolfs.Contains(OGWolfList[i]))
                    m_Wolfs.Add(OGWolfList[i]);
                timesEvaded = 0;
                panic = 10;
            }
            else if (m_Wolfs.Contains(OGWolfList[i]))
                m_Wolfs.Remove(OGWolfList[i]);
        }
    }

    void Evation()
    {
        
        panic -= 2;
        if (m_Wolfs.Count != 0)
            EvadeSomething(m_Wolfs[0]);

        else if(TileFleeTarget != null)
            EvadeSomething(TileFleeTarget);
    }

    void EvadeSomething(GameObject evadeTarget)
    {
        if (gameObject.transform.position.x < evadeTarget.transform.position.x)
        {
            //Reappearing code that checks if the new tile is preoccupied, could be optimized but because of time they are not.
            for (int i = 0; i < numberOfSheep; i++)
            {
                if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x - 1)
                {
                    isBlocked = true;
                }
            }
            //Else it moves to that tile
            if (isBlocked == false)
            {
                gameObject.transform.position += new Vector3(-1 * Mathf.RoundToInt(staminaPoints - WoolSize / 3), 0, 0);
            }
            isBlocked = false;
        }
        else if (gameObject.transform.position.x > evadeTarget.transform.position.x)
        {
            for (int i = 0; i < numberOfSheep; i++)
            {
                if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x + 1)
                {
                    isBlocked = true;
                }
            }
            if (isBlocked == false)
            {
                gameObject.transform.position += new Vector3(1 * Mathf.RoundToInt(staminaPoints - WoolSize / 3), 0, 0);
            }
            isBlocked = false;
        }

        if (gameObject.transform.position.y < evadeTarget.transform.position.y)
        {
            for (int i = 0; i < numberOfSheep; i++)
            {
                if (m_Sheep[i].transform.position.y == transform.position.y - 1 && m_Sheep[i].transform.position.x == transform.position.x)
                {
                    isBlocked = true;
                }
            }
            if (isBlocked == false)
            {
                gameObject.transform.position += new Vector3(0, -1 * Mathf.RoundToInt(staminaPoints - WoolSize / 3), 0);
            }
            isBlocked = false;
        }
        else if (gameObject.transform.position.y > evadeTarget.transform.position.y)
        {
            for (int i = 0; i < numberOfSheep; i++)
            {
                if (m_Sheep[i].transform.position.y == transform.position.y + 1 && m_Sheep[i].transform.position.x == transform.position.x)
                {
                    isBlocked = true;
                }
            }
            if (isBlocked == false)
            {
                gameObject.transform.position += new Vector3(0, 1 * Mathf.RoundToInt(staminaPoints - WoolSize / 3), 0);
            }
            isBlocked = false;
        }
        timesEvaded += 1;
        
        staminaPoints -= 0.3f;
    }

    void MoveTowardsGrass()
    {
        numberOfSheep = m_Sheep.Count;
        if (m_Grass.Count != 0 && m_Grass[0] != null)
        {
            if (gameObject.transform.position.x < m_Grass[0].transform.position.x)
            {
                //Reappearing code that checks if the new tile is preoccupied, could be optimized but because of time they are not.
                for (int i = 0; i < numberOfSheep; i++)
                {
                    if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x + 1)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(1, 0, 0);
                }
                isBlocked = false;
            }
            else if (gameObject.transform.position.x > m_Grass[0].transform.position.x)
            {
                for (int i = 0; i < numberOfSheep; i++)
                {
                    if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x - 1)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(-1, 0, 0);
                }
                isBlocked = false;
            }

            if (gameObject.transform.position.y < m_Grass[0].transform.position.y)
            {
                for (int i = 0; i < numberOfSheep; i++)
                {
                    if (m_Sheep[i].transform.position.y == transform.position.y + 1 && m_Sheep[i].transform.position.x == transform.position.x)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(0, 1, 0);
                }
                isBlocked = false;
            }
            else if (gameObject.transform.position.y > m_Grass[0].transform.position.y)
            {
                for (int i = 0; i < numberOfSheep; i++)
                {
                    if (m_Sheep[i].transform.position.y == transform.position.y - 1 && m_Sheep[i].transform.position.x == transform.position.x)
                    {
                        isBlocked = true;
                    }
                }
                if (isBlocked == false)
                {
                    gameObject.transform.position += new Vector3(0, -1, 0);
                }
                isBlocked = false;
            }

            //If the sheep have reached a target it starts to eat
            for (int i = 0; i < m_Grass.Count; i++)
            {
                if (gameObject.transform.position == m_Grass[i].transform.position)
                    EatGrass(i);
            }
        }
    }

    void EatGrass(int index)
    {
        m_Grass[index].transform.GetComponent<GrassBehaviourScript>().EatMe(eatDamage);
        healthPoints += eatDamage;
        WoolSize += 0.3f;
        //TODO: WOOLBAR
    }

    void WanderAround()
    {
        //TODO: Stamina BAR
        staminaPoints += 0.2f;
        int rand = Random.Range(1, 5);

        switch (rand)
        {
            //UP
            case 1:
                if(lastMovement == 3)
                    break;
                else
                {
                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        //Check if tile is preoccupied
                        if (m_Sheep[i].transform.position.y == transform.position.y + 1 && m_Sheep[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(0, 1, 0);
                    }
                    isBlocked = false;
                    lastMovement = 1;
                    break;
                }
            //LEFT
            case 2:
                if (lastMovement == 4)
                    break;
                else
                {
                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x - 1)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(-1, 0, 0);
                    }
                    isBlocked = false;
                    lastMovement = 2;
                    break;
                }
            //DOWN
            case 3:
                if (lastMovement == 1)
                    break;
                else
                {
                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        if (m_Sheep[i].transform.position.y == transform.position.y - 1 && m_Sheep[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(0, -1, 0);
                    }
                    isBlocked = false;
                    lastMovement = 3;
                    break;
                }
            //RIGHT
            case 4:
                if (lastMovement == 2)
                    break;
                else
                {
                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        if (m_Sheep[i].transform.position.y == transform.position.y && m_Sheep[i].transform.position.x == transform.position.x + 1)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        gameObject.transform.position += new Vector3(1, 0, 0);
                    }
                    isBlocked = false;
                    lastMovement = 4;
                    break;
                }
            default:
                lastMovement = 0;
                break;
        }
        timesWandered += 1;
        if (timesWandered >= 5)
            timesWandered = 0;
        
    }

    void GiveBirth()
    {
        healthPoints -= 50;
        int rand = Random.Range(1, 5);
        switch (rand)
        {
            case 1:
                if (transform.position.y != 0)
                {
                    tempCount = 1;

                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        //Occupation check
                        if (m_Sheep[i].transform.position.y == transform.position.y + 1 && m_Sheep[i].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewSheep();
                        m_Sheep[m_Sheep.Count - 1].transform.position = new Vector3(transform.position.x, transform.position.y + 1, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 2:
                if (transform.position.x != 0)
                {
                    tempCount = 1;

                    for (int i = 0; i < numberOfSheep; i++)
                    {
                        if (m_Sheep[i].transform.position.x == transform.position.x - 1 && m_Sheep[i].transform.position.y == transform.position.y)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewSheep();
                        m_Sheep[m_Sheep.Count - 1].transform.position = new Vector3(transform.position.x - 1, transform.position.y, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 3:
                if (transform.position.y != -(numberOfTiles - 1))
                {
                    tempCount = 1;

                    for (int j = 0; j < numberOfSheep; j++)
                    {
                        if (m_Sheep[j].transform.position.y == transform.position.y - 1 && m_Sheep[j].transform.position.x == transform.position.x)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewSheep();
                        m_Sheep[m_Sheep.Count - 1].transform.position = new Vector3(transform.position.x, transform.position.y - 1, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
            case 4:
                if (transform.position.x != (numberOfTiles - 1))
                {
                    tempCount = 1;

                    for (int j = 0; j < numberOfSheep; j++)
                    {
                        if (m_Sheep[j].transform.position.x == transform.position.x + 1 && m_Sheep[j].transform.position.y == transform.position.y)
                        {
                            isBlocked = true;
                        }
                    }
                    if (isBlocked == false)
                    {
                        m_TilePlacer.transform.GetComponent<TilePlacementScript>().SpawnNewSheep();
                        m_Sheep[m_Sheep.Count - 1].transform.position = new Vector3(transform.position.x + 1, transform.position.y, 0);
                        Debug.Log("PLOP");
                    }
                    isBlocked = false;
                }
                break;
        }
        numberOfSheep = m_Sheep.Count;
    }

    public void SetNumberOfTiles(int nmbr)
    {
        numberOfTiles = nmbr;
    }

    public List<GameObject> SetSheepList
    {
        get{ return m_Sheep; }
        set{ m_Sheep = value; }
    }

    public void EatMe(int eatDamage)
    {
        healthPoints -= eatDamage;
        gettingEaten = true;
    }

    public void DestroySheep()
    {
        Destroy(gameObject);
    }

    
}
